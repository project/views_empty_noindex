# Noindex on empty views

The "Noindex on Empty Views" module adds a noindex robots meta tag to views
that have no results.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_empty_noindex).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_empty_noindex).


## Table of contents

- Requirements
- Features
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Features

This module automatically adds a noindex robots metatag on views with no
result.
It does this only if an exposed filter was selected.

It does not handle facets as they should already not allow users to select
choices with no result.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No additional configuration is needed. Once the module is enabled, it will
automatically add a noindex robots meta tag to views with no results.


## Maintainers

- Pierre Rudloff - [prudloff](https://www.drupal.org/u/prudloff)
